/*globals angular */
var myApp = angular.module('myApp',[]);

myApp.controller('InputCtrl', ['$scope', function($scope) {
  var placeholderTemplate = 'XXXX XXXX XXXX XXXX';
  $scope.placeholder = placeholderTemplate;


  $scope.changeCardnumber = function(){
      var creditCardnumber = [];
      var placeholder = [];
      var range = Array.apply(null, new Array(16)).map(function (_, i) {return i;});

      angular.forEach(range, function(value, index){
              value = $scope.cardnumber.toString()[index];
              var valueNext = $scope.cardnumber.toString()[index-1];


              if (valueNext || index === 0) {
                  placeholder.push("");
              } else {
                  placeholder.push("X");
              }

              if (value) {
                  creditCardnumber.push(value);
              }

              if (!((index+1)%4) && index !== 0) {
                  if (value && index < 15) {
                      creditCardnumber.push("");
                  }
                  placeholder.push("");
              }
      });

      $scope.formatCreditCardNumber = creditCardnumber;
      $scope.placeholder = placeholder;
  };
}]);

myApp.controller('CalendarCtrl', ['$scope', function($scope) {
            $scope.dateCover = 'Дата окончания'

            $scope.change = function(){
                if ($scope.date.length > 4) {
                    $scope.date = "";
                }

                var dateCover = angular.copy($scope.date).split("");

                console.log(dateCover);

                if (dateCover.length > 2) {
                    dateCover.splice(2, 0, "/")
                }

                if (!dateCover.length) {
                    $scope.dateCover = 'Дата окончания'
                } else {
                    $scope.dateCover = dateCover.join("");
                }



            };
}]);
